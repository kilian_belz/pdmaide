﻿Imports EPDM.Interop.epdm
Imports System.Data.SqlClient
Imports System.IO

Module revPolice
    Public Structure CPRev
        Dim PartNumber As String
        Dim RevisionCode As Integer
    End Structure

    Sub Main()
        'Get the full file path from the command line argument specified by the PDM Dispatch Add-In
        'Assumes file path is the last command given
        Dim pdm_file_path As String = ""

        For Each arg As String In Environment.GetCommandLineArgs()
            pdm_file_path = arg
        Next

        Dim pdm_rev As String
        Dim cp_revisions As List(Of CPRev)

        'Gather revisions from PDM
        pdm_rev = GetPDMRevision(pdm_file_path)

        'Get file name
        Dim file_name As String = Path.GetFileNameWithoutExtension(pdm_file_path)
        'Get extensions without the dot
        Dim file_extension As String = Path.GetExtension(pdm_file_path).Replace(".", String.Empty)
        'Get COUNTERPART revisions for all COUNTERPART parts referencing this SW file
        cp_revisions = GetCPRevisions(file_name, file_extension)

        Dim message As String = ""

        If cp_revisions.Count > 0 Then
            For Each cp_rev In cp_revisions
                'Check that revisions match
                If Not RevisonsMatch(cp_rev.RevisionCode, pdm_rev) Then
                    message += Environment.NewLine & "COUNTERPART Part: " & cp_rev.PartNumber & "     Rev.: " & CodeToLetterRev(cp_rev.RevisionCode)
                End If
            Next
        End If

        If Not (message = "") Then
            MsgBox("WARNING - The following COUNTERPART components dependent on the SW file you just released do not have matching revisions:" & Environment.NewLine & message)
        End If
    End Sub

    Private Function GetPDMRevision(ByVal pdm_file_path As String) As String
        'Log into vault
        Dim vault As IEdmVault5 = New EdmVault5
        vault.LoginAuto("ARM", 0)

        'Dim message As String
        Dim revision As String
        Dim file As IEdmFile5

        'Get file from vault
        file = vault.GetFileFromPath(pdm_file_path)

        'Get revision
        revision = file.CurrentRevision

        Return revision
    End Function

    Private Function GetCPRevisions(ByVal pdm_file_name As String, ByVal pdm_file_extension As String) As List(Of CPRev)
        'Connect to COUNTERPART SQL database
        Dim sqlConnection1 As New SqlConnection("server=ZR1;database=COUNTERPART;Trusted_Connection=Yes;")
        Dim cmd As New SqlCommand
        Dim reader As SqlDataReader

        Console.WriteLine(pdm_file_name)
        Console.WriteLine(pdm_file_extension)

        'Query
        cmd.CommandText = "
            select
	            Component.Name as 'Part Number',
                ComponentRevision.Code as 'Revision'
            from Component
	            inner join Document
		            on Component.DocumentId=Document.Id
	            inner join DocumentExtension
		            on Document.ExtensionId=DocumentExtension.Id
	            inner join ComponentRevision
		            on Component.LatestRevisionId=ComponentRevision.Id
            where
	            Document.Name like '" & pdm_file_name & "' and DocumentExtension.Name like '" & pdm_file_extension & "'
        "
        cmd.CommandType = CommandType.Text

        'Open connection
        cmd.Connection = sqlConnection1
        sqlConnection1.Open()

        'Execute query
        reader = cmd.ExecuteReader()

        Dim revision_entry As CPRev
        Dim cp_revisions As New List(Of CPRev)

        'If COUNTERPART components referencing SW file are found enter all in the list
        If reader.HasRows Then
            Do While reader.Read()
                revision_entry.PartNumber = reader.GetString(0)
                revision_entry.RevisionCode = reader.GetInt32(1)

                cp_revisions.Add(revision_entry)
            Loop
        Else
            MsgBox("Released part not found in COUNTERPART!")
        End If

        reader.Close()

        sqlConnection1.Close()

        Return cp_revisions
    End Function

    Private Function RevisonsMatch(ByVal cp_rev As Int32, ByVal pdm_rev As String) As Boolean
        Dim revision_code As Integer

        'Get revision code from PDM revision letter
        revision_code = LetterRevToCode(pdm_rev)

        'Check to make sure PDM and COUNTERPART revisions match
        If (cp_rev = revision_code) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function LetterRevToCode(ByVal pdm_rev As String) As Integer
        'Check that revision exists and is not longer than 1 character (should support double character rev's in the future)
        If (pdm_rev.Length > 1 Or pdm_rev.Length = 0) Then
            Return 0
        Else
            'Turn letter into COUNTERPART code for rev's (letters numbered alphabetically; A is 1, B is 2, etc.)
            Return (Asc(pdm_rev.Chars(0)) - Asc("A")) + 1
        End If
    End Function

    Private Function CodeToLetterRev(ByVal cp_rev_code As Integer) As Char
        'Check that code is within the 26 letters of the alphabet; if so return letter
        If (0 <= cp_rev_code <= 26) Then
            Return Chr(cp_rev_code + Asc("A") - 1)
        Else
            Return ""
        End If
    End Function

End Module
